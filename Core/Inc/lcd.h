//********************* (C) COPYRIGHT 2015 Technomaster ************************
/// @brief		Header das fun��es de acesso ao LCD
/// @file 		lcd.h
/// @author		Paulo Steigleder
/// @version	V1.00
/// @date		21/12/2015
// *****************************************************************************

// Preven��o contra inclus�o recursiva -----------------------------------------
#ifndef __LCD_H
#define __LCD_H

// Includes --------------------------------------------------------------------
// Exported types --------------------------------------------------------------

// Exported constants ----------------------------------------------------------
#define COL_NUMBER		16
#define ROW_NUMBER		2
#define LINE_1			0
#define LINE_2			1

#define ENTER			0xFC
#define INC				0xFD
#define DEC				0xFE
#define FULL			0xFF

// Exported macro --------------------------------------------------------------

// Exported functions ----------------------------------------------------------
void LcdInit(void);
void LcdUpdate(u8 *pu8Text);
void LcdPrintf(u8 *pu8Dest, u8 u8Line, const char *fmt, ...);

#endif

//**************** (C) COPYRIGHT 2015 Technomaster *******END OF FILE***********
