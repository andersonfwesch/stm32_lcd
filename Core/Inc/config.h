//********************* (C) COPYRIGHT 2015 Technomaster ************************
/// @brief		Header para a inicializa��o do sistema
/// @file 		config.h
/// @author		Paulo Steigleder
/// @version	V1.0.0
/// @date		21/12/2015
// *****************************************************************************

// Preven��o contra inclus�o recursiva -----------------------------------------
#ifndef __CONFIG_H
#define __CONFIG_H

#include "stm32f1xx_hal.h"
#include "system.h"

// Includes --------------------------------------------------------------------

// Exported constants ----------------------------------------------------------
#define VERSION 	 		"1.03"
#define SYSTEM_FREQUENCY	24000000	// frequ�ncia de clock do sistema (Hz)

enum
{
	VIN,
	TSYS,
	VSYS,
	NUM_AD_CHANNELS
};

// Porta A
#define VIN_PIN				GPIO_PIN_0
#define DIR_TX_PIN			GPIO_PIN_1
#define USART2_TX_PIN		GPIO_PIN_2
#define USART2_RX_PIN		GPIO_PIN_3
#define LCD_RS_PIN			GPIO_PIN_4
#define LCD_RD_PIN			GPIO_PIN_5
#define LCD_CTR_PIN			GPIO_PIN_6
#define LCD_BKL_PIN			GPIO_PIN_7
#define RELAY2_PIN			GPIO_PIN_8
#define USART1_TX_PIN		GPIO_PIN_9
#define USART1_RX_PIN		GPIO_PIN_10
#define RELAY1_PIN			GPIO_PIN_11
#define INPUTA_PIN			GPIO_PIN_15

// Porta B
#define LCD_ENB_PIN			GPIO_PIN_0
#define BEEP_PIN			GPIO_PIN_4
#define KEY1_PIN			GPIO_PIN_5
#define KEY2_PIN			GPIO_PIN_6
#define KEY3_PIN			GPIO_PIN_7
#define LCD_BUS_PINS		(GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15)
#define BT1_Pin 			GPIO_PIN_2

//Porta C
#define LED1_PIN			GPIO_PIN_13
#define LED2_PIN	 		GPIO_PIN_14
#define LED1_PIN_GPIO_Port 	GPIOC
#define LED2_PIN_GPIO_Port	GPIOC

//Porta D
#define INPUTB_PIN			GPIO_PIN_0

//#define LED1_PIN GPIO_PIN_13
//#define LED1_PIN_GPIO_Port GPIOC
//#define LED2_PIN GPIO_PIN_14
//#define LED2_PIN_GPIO_Port GPIOC
//#define INPUTB_PIN GPIO_PIN_0
//#define GPIO_Pin_0_GPIO_Port GPIOD
//#define VIN_PIN GPIO_PIN_0
//#define VIN_PIN_GPIO_Port GPIOA
//#define DIR_TX_PIN GPIO_PIN_1
//#define DIR_TX_PIN_GPIO_Port GPIOA
//#define USART2_TX_PIN GPIO_PIN_2
//#define USART2_TX_PIN_GPIO_Port GPIOA
//#define USART2_RX_PIN GPIO_PIN_3
//#define USART2_RX_PIN_GPIO_Port GPIOA
//#define LCD_RS_PIN GPIO_PIN_4
//#define LCD_RS_PIN_GPIO_Port GPIOA
//#define LCD_RD_PIN GPIO_PIN_5
//#define LCD_RD_PIN_GPIO_Port GPIOA
//#define LCD_CTR_PIN GPIO_PIN_6
//#define LCD_CTR_PIN_GPIO_Port GPIOA
//#define LCD_BKL_PIN GPIO_PIN_7
//#define LCD_BKL_PIN_GPIO_Port GPIOA
//#define LCD_ENB_PIN GPIO_PIN_0
//#define LCD_ENB_PIN_GPIO_Port GPIOB
//#define BT__Pin GPIO_PIN_2
//#define BT__GPIO_Port GPIOB
//#define LCD_BUS_PINS_Pin GPIO_PIN_9
//#define LCD_BUS_PINS_GPIO_Port GPIOB
//#define RELAY2_PIN GPIO_PIN_8
//#define RELAY2_PIN_GPIO_Port GPIOA
//#define USART1_TX_PIN GPIO_PIN_9
//#define USART1_TX_PIN_GPIO_Port GPIOA
//#define USART1_RX_PIN GPIO_PIN_10
//#define USART1_RX_PIN_GPIO_Port GPIOA
//#define RELAY1_PIN GPIO_PIN_11
//#define RELAY1_PIN_GPIO_Port GPIOA
//#define TMS_Pin GPIO_PIN_13
//#define TMS_GPIO_Port GPIOA
//#define TCK_Pin GPIO_PIN_14
//#define TCK_GPIO_Port GPIOA
//#define INPUTA_PIN GPIO_PIN_15
//#define INPUTA_PIN_GPIO_Port GPIOA
//#define TDO_Pin GPIO_PIN_3
//#define TDO_GPIO_Port GPIOB
//#define BEEP_PIN GPIO_PIN_4
//#define BEEP_PIN_GPIO_Port GPIOB
//#define KEY1_PIN GPIO_PIN_5
//#define KEY1_PIN_GPIO_Port GPIOB
//#define KEY2_PIN GPIO_PIN_6
//#define KEY2_PIN_GPIO_Port GPIOB
//#define KEY3_PIN GPIO_PIN_7
//#define KEY3_PIN_GPIO_Port GPIOB
// Exported types --------------------------------------------------------------

// Exported macro --------------------------------------------------------------
#define DirTx(_a)			(HAL_GPIO_WritePin (GPIOA, DIR_TX_PIN, (GPIO_PinState)_a))
#define	LcdRs(_a)			(HAL_GPIO_WritePin(GPIOA, LCD_RS_PIN, (GPIO_PinState)_a))
#define	LcdRw(_a)			(HAL_GPIO_WritePin(GPIOA, LCD_RD_PIN, (GPIO_PinState)_a))
#define Relay1On(_a)		(HAL_GPIO_WritePin(GPIOA, RELAY1_PIN, (GPIO_PinState)_a))
#define Relay2On(_a)		(HAL_GPIO_WritePin(GPIOA, RELAY2_PIN, (GPIO_PinState)_a))

#define IsInputAOn()		((HAL_GPIO_ReadPin(GPIOA, INPUTA_PIN) == 0) ? TRUE : FALSE)
#define IsInputBOn()		((HAL_GPIO_ReadPin(GPIOD, INPUTB_PIN) == 0) ? TRUE : FALSE)

#define	LcdEnb(_a)			(HAL_GPIO_WritePin(GPIOB, LCD_ENB_PIN, (GPIO_PinState)_a))
#define BeepOn(_a)			(HAL_GPIO_WritePin(GPIOB, BEEP_PIN, (GPIO_PinState)_a))
#define IsKey1On()			((HAL_GPIO_ReadPin(GPIOB, KEY1_PIN) == 0) ? TRUE : FALSE)
#define IsKey2On()			((HAL_GPIO_ReadPin(GPIOB, KEY2_PIN) == 0) ? TRUE : FALSE)
#define IsKey3On()			((HAL_GPIO_ReadPin(GPIOB, KEY3_PIN) == 0) ? TRUE : FALSE)
#define KeyRead()			((~HAL_GPIO_ReadPort(GPIOB) >> 5) & 0x0007)

#define Led1On(_a)			(HAL_GPIO_WritePin(GPIOC, LED1_PIN, (GPIO_PinState)_a))
#define Led2On(_a)			(HAL_GPIO_WritePin(GPIOC, LED2_PIN, (GPIO_PinState)_a))

#define LcdBusRd()			(HAL_GPIO_ReadPort(GPIOB) >> 8)
#define LcdBusWr(_a)		(HAL_GPIO_WritePort(GPIOB, (_a << 8) | (HAL_GPIO_ReadPort(GPIOB) & 0x0011) | 0x00E0))

// Exported functions ----------------------------------------------------------
u16 ReadAnalogIn(u8 u8Channel);
void ProtectFlash(void);
void SystemInit(void);
void ConfigHw(void);
void SendData(USART_TypeDef* USARTx, u8 u8Data);


#endif

//**************** (C) COPYRIGHT 2015 Technomaster *******END OF FILE***********

