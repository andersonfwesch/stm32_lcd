//***************** (C) COPYRIGHT 2013 Vieira Filho Tecnologia *****************
/// @brief		Header das fun��es do sistema
/// @file 		system.h
/// @author		Paulo Steigleder
/// @author		Roberto Gomes
/// @version	V6.0.2
/// @date		08/03/2016
// *****************************************************************************

// Preven��o contra inclus�o recursiva -----------------------------------------
#ifndef __SYSTEM_H
#define __SYSTEM_H

// Includes --------------------------------------------------------------------
// Exported types --------------------------------------------------------------

// Exported constants ----------------------------------------------------------
#define UID_SIZE	12

// Exported macro --------------------------------------------------------------

// Exported functions ----------------------------------------------------------
void IncSysTime(void);
u32 ReadSysTime(void);
u32 TimeDiff(u32 u32OldTime);
void Delay_us(u32 u32us);
void Delay_ms(u32 u32Timeout);
void BlinkControl(void);
u8 IsBlink(void);
void GetUniqueID(u8 *pu8Pointer, u8 u8Size);
void ReadAD(void);
float GetAD(u8 u8Channel);
void BacklightControl(u8 u8Value);
void ContrastControl(u8 u8Value);
u16 HAL_GPIO_ReadPort(GPIO_TypeDef* GPIOx);
void HAL_GPIO_WritePort(GPIO_TypeDef* GPIOx, u16 PortVal);

#endif

//********* (C) COPYRIGHT 2016 Vieira Filho Tecnologia *****END OF FILE*********
