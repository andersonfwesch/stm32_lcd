//********************* (C) COPYRIGHT 2015 Technomaster ************************
/// @brief		Arquivos de inicializa��o do sistema
/// @file 		config.c
/// @author		Paulo Steigleder
/// @version	V1.0.0
/// @date		21/12/2015
// *****************************************************************************

// Includes --------------------------------------------------------------------
#include "stm32f1xx_hal.h"
#include "config.h"
#include "system.h"
//#include "flash.h"



// Private typedef -------------------------------------------------------------
// Private define --------------------------------------------------------------
#define AD_DR_ADDRESS		((u32)0x4001244C)

// Private macro ---------------------------------------------------------------

// Private variables -----------------------------------------------------------
vu16 vu16ADConvertedValue[NUM_AD_CHANNELS];

// Private function prototypes -------------------------------------------------
void ConfigRCC(void);
void ConfigNVIC(void);
void ConfigIO(void);
void ConfigWDT(void);
void ConfigTIM(void);
void ConfigDMA(void);
void ConfigADC(void);
void ConfigUSART(void);

// Private functions -----------------------------------------------------------
// *****************************************************************************
/// @brief		L� o valor de um canal anal�gico
/// @fn			u16 ReadAnalogIn(u8 u8Channel)
// *****************************************************************************
u16 ReadAnalogIn(u8 u8Channel)
{
	return vu16ADConvertedValue[u8Channel];
}

// *****************************************************************************
/// @brief		Configura o hardware
/// @fn			void ConfigHw(void)
// *****************************************************************************
void ConfigHw(void)
{
	//ConfigRCC();			// configura os clocks do sistema
	ConfigNVIC();			// configura o controlador de interrup��o
	ConfigIO();				// configura entradas e sa�das
//	ConfigADC();			// configura o conversor AD
//	ConfigDMA();			// configura o acesso direto � mem�ria
//	ConfigUSART();			// configura a interface USART1
//	ConfigTIM();			// configura o temporizador do filtro
//	ReadSysConfig();		// carrega as vari�veis retentivas
//	ConfigWDT();			// configura o c�o de guarda
//	ProtectFlash();			// configura a prote��o � leitura da mem�ria flash
}

// *****************************************************************************
/// u@brief		Inicializa��o dos clocks do sistema
/// @fn			void SystemInit(void)
// *****************************************************************************
//void SystemInit(void)
//{
//	RCC_DeInit();
//	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);		// habilita o buffer de prefetch
//	FLASH_SetLatency(FLASH_Latency_0);							// ajusta a lat�ncia da mem�ria flash para 1 wait state
//	RCC_HCLKConfig(RCC_SYSCLK_Div1);
//	RCC_PCLK2Config(RCC_HCLK_Div1);
//	RCC_PCLK1Config(RCC_HCLK_Div1);
//	RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_6);		// PLLCLK = 4 MHz x 6 = 24 MHz
//	RCC_PLLCmd(ENABLE);											// habilita o PLL
//	while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);		// aguarda o PLL estar pronto
//	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);					// seleciona o PLL como fonte de clock
//	while(RCC_GetSYSCLKSource() != 0x08);						// aguarda a comuta��o
//	RCC_ADCCLKConfig(RCC_PCLK2_Div8);							// clock ADC = 24 MHz / 8 = 3 MHz
//	NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0000);			// seta o endere�o base em 0x08000000
//}

// *****************************************************************************
/// @brief		Configura os v�rios sistemas de clock
/// @fn			void ConfigRCC(void)
// *****************************************************************************
//void ConfigRCC(void)
//{
//    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_SRAM | RCC_AHBPeriph_DMA1 | RCC_AHBPeriph_CRC, ENABLE);
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC |
//							RCC_APB2Periph_GPIOD | RCC_APB2Periph_USART1 | RCC_APB2Periph_ADC1 |
//							RCC_APB2Periph_AFIO | RCC_APB2Periph_TIM16 | RCC_APB2Periph_TIM17, ENABLE);
//	SysTick_Config(SYSTEM_FREQUENCY / 1000);
//}

// *****************************************************************************
/// @brief		Configura as interrup��es
/// @fn			void ConfigNVIC(void)
// *****************************************************************************
void ConfigNVIC(void)
{

	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_0);				// configura os bits de prioridade
	HAL_NVIC_SetPriority(SysTick_IRQn, 2, 0);							// altera a prioridade de interrup��o do rel�gio do sistema

	HAL_NVIC_SetPriority(USART2_IRQn, 1, 0);			// habilita a interrup��o da recep��o da USART2 - Modbus
}

// *****************************************************************************
/// @brief		Configura as portas de entrada e sa�da
/// @fn			void ConfigGPIO(void)
// *****************************************************************************
void ConfigIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
  
	DirTx(FALSE);
	Relay1On(FALSE);
	Relay2On(FALSE);
	LcdEnb(FALSE);
	BeepOn(FALSE);
	Led1On(FALSE);
	Led2On(FALSE);

    __HAL_RCC_USART2_CLK_ENABLE();
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

//	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
//	GPIO_PinRemapConfig(GPIO_Remap_TIM16, ENABLE);
//	GPIO_PinRemapConfig(GPIO_Remap_TIM17, ENABLE);
	
	GPIO_InitStructure.Pin = VIN_PIN;								// leitura de tens�o de entrada
	GPIO_InitStructure.Mode = GPIO_MODE_AF_INPUT;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = DIR_TX_PIN | LCD_RS_PIN | LCD_RD_PIN | RELAY1_PIN | RELAY2_PIN;	// sa�da de controle do transceptor RS-485
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = USART2_RX_PIN | USART1_RX_PIN | INPUTA_PIN;						// entradas das USART1, USART2 e externa A
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.Pin = USART2_TX_PIN | LCD_CTR_PIN | LCD_BKL_PIN | USART1_TX_PIN; 		// sa�das da USART1, USART2, contraste e brilho do LCD
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = LCD_ENB_PIN | BEEP_PIN;				// sa�das de habilita��o do LCD e sinalizador ac�stico
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = KEY1_PIN | KEY2_PIN | KEY3_PIN;		// entradas das teclas
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = LED1_PIN | LED2_PIN;					// sa�das dos LEDs de sinaliza��o
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_InitStructure.Pin = INPUTB_PIN;							// entrada externa B
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
}

// *****************************************************************************
/// @brief		Configura e habilita o c�o de guarda
/// @fn			void ConfigWDT(void)
// *****************************************************************************
//void ConfigWDT(void)
//{
//	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);		// habilita a escrita no c�o de guarda
//	IWDG_SetPrescaler(IWDG_Prescaler_16);				// ajusta o divisor do c�o de guarda (40 kHz / 16 = 2,5 kHz)
// 	IWDG_SetReload(2500);						 		// carrega do valor do contador (tempo resultante 1 s)
//	IWDG_ReloadCounter();								// recarrega o c�o de guarda
// 	IWDG_Enable();										// habilita o c�o de guarda
//	IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
//}

// *****************************************************************************
/// @brief		Configura o temporizador
/// @fn			void ConfigTIM(void)
// *****************************************************************************
//void ConfigTIM(void)
//{
//	TIM_TimeBaseInitTypeDef	TIM_TimeBaseStructure;
//	TIM_OCInitTypeDef TIM_OCInitStructure;
//
//	// Configura��o do TIM16 (gera��o de um sinal PWM do contraste do LCD)
//	// TIM16CLK = 24 MHz, Prescaler = 119, TIM16 counter clock = 200 kHz
//	// TIM3 ARR Register = 99 => TIM16 Frequency = TIM16 counter clock / (ARR + 1)
//	// TIM3 Frequency = 2 kHz
//
//	TIM_TimeBaseStructure.TIM_Period = 99;							// configura��o da base de tempo
//	TIM_TimeBaseStructure.TIM_Prescaler = 119;
//	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;;
//	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
//	TIM_TimeBaseInit(TIM16, &TIM_TimeBaseStructure);
//
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;				// par�metros do canal 1
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure.TIM_Pulse = 0;
//	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
//	TIM_OC1Init(TIM16, &TIM_OCInitStructure);						// configura��o do PWM do canal 1
//
//	TIM_OC1PreloadConfig(TIM16, TIM_OCPreload_Enable);
//	TIM_Cmd(TIM16, ENABLE);
//	TIM_CtrlPWMOutputs(TIM16, ENABLE);
//
//	// Configura��o do TIM17 (gera��o de um sinal PWM do backlight do LCD)
//	// TIM17CLK = 24 MHz, Prescaler = 23, TIM17 counter clock = 1 MHz
//	// TIM17 ARR Register = 99 => TIM17 Frequency = TIM17 counter clock / (ARR + 1)
//	// TIM17 Frequency = 10 kHz
//
//	TIM_TimeBaseStructure.TIM_Period = 99;							// configura��o da base de tempo
//	TIM_TimeBaseStructure.TIM_Prescaler = 23;
//	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;;
//	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
//	TIM_TimeBaseInit(TIM17, &TIM_TimeBaseStructure);
//
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;				// par�metros do canal 1
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure.TIM_Pulse = 0;
//	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
//	TIM_OC1Init(TIM17, &TIM_OCInitStructure);						// configura��o do PWM do canal 1
//
//	TIM_OC1PreloadConfig(TIM17, TIM_OCPreload_Enable);
//	TIM_Cmd(TIM17, ENABLE);
//	TIM_CtrlPWMOutputs(TIM17, ENABLE);
//}
	
// *****************************************************************************
/// @brief		Configura o acesso direto � mem�ria
/// @fn			void ConfigDMA(void)
// *****************************************************************************
//void ConfigDMA(void)
//{
//	DMA_InitTypeDef	DMA_InitStructure;
//
//	DMA_DeInit(DMA1_Channel1);
//	DMA_InitStructure.DMA_PeripheralBaseAddr = AD_DR_ADDRESS;						// define o endere�o do conversor AD
//	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;								// define o conversor AD como fonte
//	DMA_InitStructure.DMA_MemoryBaseAddr = (u32)vu16ADConvertedValue;				// define o endere�o do buffer de dados
//	DMA_InitStructure.DMA_BufferSize = NUM_AD_CHANNELS;								// define o tamanho do buffer de dados
//	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;				// desabilita o incremento do endere�o do perif�rico
//	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;							// habilita o incremento do endere�o do buffer
//	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;		// define o barramento de dados do conversor AD como 16 bits
//	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;				// define o barramento de dados do buffer como 16 bits
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;									// define o buffer como circular
//	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;								// define a prioridade como baixa
//	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;									// desabilita a transfer�ncia mem�ria a mem�ria
//	DMA_Init(DMA1_Channel1, &DMA_InitStructure);		 							// inicializa o canal 1 do DMA1 (conversor AD)
//	DMA_Cmd(DMA1_Channel1, ENABLE);													// habilita o canal 1 do DMA1 (conversor AD)
//}

// *****************************************************************************
/// @brief		Configura o conversor AD
/// @fn			void ConfigADC(void)
// *****************************************************************************
//void ConfigADC(void)
//{
//	ADC_InitTypeDef	ADC_InitStructure;
//
//	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;								// configura��o do conversor AD
//	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
//	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
//	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
//	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
//	ADC_InitStructure.ADC_NbrOfChannel = NUM_AD_CHANNELS;
//	ADC_Init(ADC1, &ADC_InitStructure);
//	ADC_TempSensorVrefintCmd(ENABLE);
//
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_239Cycles5);		// tens�o de alimenta��o
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_16, 2, ADC_SampleTime_239Cycles5);		// temperatura
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_17, 3, ADC_SampleTime_239Cycles5);		// tens�o de refer�ncia
//
//	ADC_DMACmd(ADC1, ENABLE);															// habilita o DMA do conversor AD
//	ADC_Cmd(ADC1, ENABLE);																// habilita o conversor AD
//	ADC_ResetCalibration(ADC1);															// habilita o registro do reset da calibra��o do conversor AD
//	while (ADC_GetResetCalibrationStatus(ADC1));										// aguarda o final do reset da calibra��o do conversor AD
//	ADC_StartCalibration(ADC1);															// inicia a calibra��o do conversor AD
//	while (ADC_GetCalibrationStatus(ADC1));												// aguarda o final da calibra��o do conversor AD
//	ADC_SoftwareStartConvCmd(ADC1, ENABLE);												// habilita a convers�o AD
//}

// *****************************************************************************
/// @brief		Configura a porta de comunica��o serial
/// @fn			void ConfigUSART1(void)
// *****************************************************************************
//void ConfigUSART(void)
//{
//	USART_InitTypeDef USART_InitStructure;
//
//	USART_InitStructure.USART_WordLength = USART_WordLength_8b;							// n�mero de bits de dados = 8
//	USART_InitStructure.USART_StopBits = USART_StopBits_1;								// n�mero de bits de final = 1
//	USART_InitStructure.USART_Parity = USART_Parity_No;									// sem paridade
//	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;		// sem controle de fluxo
//	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;						// modo duplex
// 	USART_InitStructure.USART_BaudRate = 115200;										// velocidade
//	USART_DeInit(USART1);
//	USART_Init(USART1, &USART_InitStructure);											// configura a USART1
//	USART_Cmd(USART1, ENABLE);
//
//	USART_InitStructure.USART_WordLength = USART_WordLength_8b;							// n�mero de bits de dados = 8
//	USART_InitStructure.USART_StopBits = USART_StopBits_1;								// n�mero de bits de final = 1
//	USART_InitStructure.USART_Parity = USART_Parity_No;									// paridade par
//	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;		// sem controle de fluxo
//	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;						// modo duplex
// 	USART_InitStructure.USART_BaudRate = 115200;										// velocidade
//	USART_DeInit(USART2);
//	USART_Init(USART2, &USART_InitStructure);											// configura a USART2
//	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);										// habilita a interrup��o da recep��o
//	USART_Cmd(USART2, ENABLE);
//}

// *****************************************************************************
// *****************************************************************************
//void SendData(USART_TypeDef* USARTx, u8 u8Data)
//{
//	while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
//	USARTx->DR = u8Data;
//}

//**************** (C) COPYRIGHT 2015 Technomaster *******END OF FILE***********
