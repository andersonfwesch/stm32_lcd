//********************* (C) COPYRIGHT 2015 Technomaster ************************
/// @brief		Fun��es de acesso ao visor LCD
/// @file 		lcd.c
/// @author		Paulo Steigleder
/// @version	V1.00
/// @date		21/12/2015
// *****************************************************************************

// Includes --------------------------------------------------------------------
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "stm32f1xx_hal.h"
#include "config.h"
#include "lcd.h"
//#include "system.h"
#include "special_char.h"

// Private typedef -------------------------------------------------------------
typedef enum
{
	LCD_WRITE,
	LCD_READ
} eLCD_RW;

typedef enum
{
	LCD_COMMAND,
	LCD_DATA
} eLCD_RS;

// Private define --------------------------------------------------------------
#define LCD_CLR			0x01			// limpa todo o visor
#define LCD_HOME		0x02			// coloca cursor na posi��o inicial

#define LCD_SET			0x04			// define a dire��o de deslocamento do cursor e especifica deslocamento ou exibi��o
#define LCD_NO_INC		0x00			// desabilita incremento
#define LCD_INC			0x02			// habilita incremento
#define LCD_NO_SHIFT	0x00			// deslocamento
#define LCD_SHIFT		0x01			// desloca visor completo

#define LCD_DISPLAY		0x08			// controle do visor, cursor e atributo (piscante)
#define LCD_DISP_OFF	0x08			// desabilita visor
#define LCD_DISP_ON		0x0C			// habilita visor
#define LCD_CURS_OFF	0x00			// desabilita cursor
#define LCD_CURS_ON		0x02			// habilita cursor
#define LCD_BLINK_OFF	0x00			// desabilita cursor piscante
#define LCD_BLINK_ON	0x01			// habilita cursor piscante

#define LCD_CURS		0x10			// move cursor e desloca visor
#define LCD_CURS_SHIFT	0x00			// desloca cursor
#define LCD_DISP_SHIFT	0x08			// desloca visor
#define LCD_RIGHT		0x04			// cursor/visor � direita
#define LCD_LEFT		0x00			// cursor/visor � esquerda

#define LCD_FCTSET		0x20			// define modo 4 bits, n�mero de linhas, n�mero de pixels do caractere
#define LCD_4BIT		0x00			// comando para modo 4 bits
#define LCD_8BIT		0x10			// comando para modo 8 bits
#define LCD_1_LINE		0x00			// define modo 1 linha
#define LCD_2_LINE		0x08			// define modo 2 linhas
#define LCD_5x8			0x00			// define modo 5x8 pixels
#define LCD_5x10		0x04			// define modo 5x10 pixels

#define LCD_CGRAM		0x40			// seta endere�o da CG RAM
#define LCD_DDRAM		0x80			// seta endere�o da DD RAM
#define LCD_LINE_OFFSET	0x40			// offset do endere�o de linha
#define	LCD_BUSY		0x80

#define SPECIAL_CHAR_SIZE	8

// Private macro ---------------------------------------------------------------

// Private variables -----------------------------------------------------------
u8 u8Array[SPECIAL_CHAR_SIZE];

// Private function prototypes -------------------------------------------------
void DataBusDir(bool bDir);
u8 LcdRead(void);
void LcdWrite(u8 u8Type, u8 u8Data);
bool IsLcdBusy(void);
u8 FilterSpecialChar(u8 u8Char);
void LcdSpecCharGen(u8 *pu8Text);
void Centralize(u8 u8Line, u8 *pu8NumChar);

// Private functions -----------------------------------------------------------
// *****************************************************************************
/// @brief		Controla a dire��o do barramento de dados
/// @fn			void DataBusDir(bool bDir)
/// @param[in]	bDir		@brief  FALSE = entrada, TRUE = sa�da
// *****************************************************************************
void DataBusDir(bool bDir)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	if (bDir == FALSE)										// verifica se � para definir como entrada
		GPIO_InitStructure.Mode = GPIO_MODE_INPUT;		// se �, configura os pinos como entrada com pull-up
	else													// se n�o �,
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;	// configura os pinos como sa�da push-pull
	GPIO_InitStructure.Pin = LCD_BUS_PINS;
	GPIO_InitStructure.Speed = GPIO_SPEED_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
}

// *****************************************************************************
/// @brief		L� um byte do display LCD
/// @fn			u8 LcdRead(void)
/// @retval		Valor lido do LCD
// *****************************************************************************
u8 LcdRead(void)
{
	u8 u8Result;

	LcdRw(LCD_READ);										// define a opera��o como leitura
	LcdEnb(TRUE);											// ativa o sinal de habilita��o do display LCD
	DataBusDir(FALSE);										// coloca o barramento em modo entrada
	u8Result = LcdBusRd();									// l� o valor do barramento
	LcdEnb(FALSE);											// desativa o sinal de habilita��o do display LCD
	return u8Result;
}

// *****************************************************************************
/// @brief		Escreve um byte no display LCD
/// @fn			void LcdWrite(u8 u8Type, u8 u8Data)
/// @param[in]	u8Type		@brief Comando ou dado
/// @param[in]	u8Data		@brief Dado a ser escrito
// *****************************************************************************
void LcdWrite(u8 u8Type, u8 u8Data)
{
	while (IsLcdBusy());
	LcdRw(LCD_WRITE);										// define a opera��o como escrita
	LcdRs(u8Type);
	DataBusDir(TRUE);										// coloca o barramento em modo sa�da
	LcdBusWr(u8Data);										// coloca o valor no barramento
	LcdEnb(TRUE);											// gera o pulso de habilita��o do display
	Delay_us(1);											// 1 �s
	LcdEnb(FALSE);
	Delay_us(5);											// 5 �s
}

// *****************************************************************************
/// @brief		Fun��o que verifica se o visor LCD est� ocupado
/// @fn			bool IsLcdBusy(void)
// *****************************************************************************
bool IsLcdBusy(void)
{
	bool bLcdBusy;
	u8 u8Status;

	LcdRs(LCD_COMMAND);										// coloca o visor em modo comando
	DataBusDir(FALSE);										// coloca o barramento em modo entrada
	u8Status = LcdRead();
	bLcdBusy = (u8Status & LCD_BUSY) ? TRUE : FALSE;
	return bLcdBusy;
}

// *****************************************************************************
/// @brief		Inicializa os pinos de controle dos displays LCD
/// @fn			void LcdInit(void)
// *****************************************************************************
void LcdInit(void)
{
	LcdEnb(RESET);
	LcdRw(LCD_WRITE);
	LcdRs(LCD_COMMAND);
	HAL_Delay(40);													// aguarda o tempo de reset do visor LCD
	LcdWrite(LCD_COMMAND, LCD_FCTSET | LCD_8BIT | LCD_2_LINE | LCD_5x8);	// define modo 8 bits e duas linhas
	Delay_us(60);
	LcdWrite(LCD_COMMAND, LCD_DISPLAY | LCD_DISP_ON | LCD_BLINK_OFF);		// define o controle do visor- liga o display, sem cursor, cursor n�o piscante
	Delay_us(60);
	LcdWrite(LCD_COMMAND, LCD_CLR);
	HAL_Delay(2);
	LcdWrite(LCD_COMMAND, LCD_SET | LCD_INC | LCD_NO_SHIFT);				// define o modo de acesso - incremento autom�tico, sem deslocamento
	HAL_Delay(2);
}

// *****************************************************************************
/// @brief		Fun��o que filtra os caracteres especiais
/// @fn			u8 FilterSpecialChar(u8 u8Char)
/// @param[in]	u8Char		@brief Caractere
// *****************************************************************************
u8 FilterSpecialChar(u8 u8Char)
{
	u8 n;

	if ((u8Char >= FIRST_STD_CHAR) && (u8Char <= LAST_STD_CHAR))
		return u8Char;
	for (n = 0; n < SPECIAL_CHAR_SIZE; n++)									// pesquisa se o caractere especial foi definido
   		if (u8Array[n] == u8Char)
   			break;
	if( (n == SPECIAL_CHAR_SIZE) || (u8Char == '\0'))						// se n�o foi,
		return ' ';															// escreve espa�o em branco no visor
	else																	// se foi,
		return n;															// escreve o caractere especial no visor
}	

// *****************************************************************************
/// @fn			void LcdSpecCharGen(u8 u8Number, u8 *pu8Text)
/// @brief		Fun��o que gera os caracteres especiais na mem�ria CGRAM do LCD
/// @param[in]	pu8Text		@brief Ponteiro para o texto
// *****************************************************************************
void LcdSpecCharGen(u8 *pu8Text)
{
	u8 n, m, u8Index, u8Value;

	u8Index = 0;
	memset(&u8Array[0], 0, sizeof(u8Array));									// limpa a tabela de caracteres especiais
	for (n = 0; n < (COL_NUMBER * ROW_NUMBER); n++, *pu8Text++)					// pesquisa por caracteres especiais em todo o visor
	{
 		for (m = 0; (u8Array[m] != *pu8Text) && (m < SPECIAL_CHAR_SIZE); m++);	// pesquisa se o caractere j� foi definido
 		if ((*pu8Text >= FIRST_SPC_CHAR) && (m == SPECIAL_CHAR_SIZE))			// verifica se � caractere especial e n�o foi definido
		{
 			u8Value = uc8CharLookupTable[*pu8Text - FIRST_SPC_CHAR];			// busca caractere na tabela
			if (u8Value != 0xFF)												// verifica se caractere � v�lido
			{
				LcdWrite(LCD_COMMAND, LCD_CGRAM + (u8Index << 3));				// envia comando de escrita na CGRAM
				for (m = 0; m < 8; m++, u8Value++)
					LcdWrite(LCD_DATA, uc8ExtAsciiTable[u8Value]);				// monta o caractere especial na CGRAM
				u8Array[u8Index] = *pu8Text;									// coloca o valor do caractere na tabela de especiais
				u8Index++;														// incrementa ponteiro da tabela
				if (u8Index == SPECIAL_CHAR_SIZE)								// verifica se chegou ao final da tabela
					break;
			}
		}
	}
	LcdWrite(LCD_DDRAM, LCD_COMMAND);											// envia comando de escrita na CGRAM
}

// *****************************************************************************
/// @brief		Fun��o que transfere da mem�ria de texto para o visor LCD
/// @fn			void LcdUpdate(u8 *pu8Text)
/// @param[in]	pu8Text		@brief Ponteiro para o texto
// *****************************************************************************
void LcdUpdate(u8 *pu8Text)
{
	u8 u8Row, u8Col;
	static u32 u32LcdUpdateTimer = 0;

	if (TimeDiff(u32LcdUpdateTimer) > 100)
	{
		u32LcdUpdateTimer = HAL_GetTick();
		LcdSpecCharGen(pu8Text);				
		for (u8Row = 0; u8Row < ROW_NUMBER; u8Row++)
		{
			LcdWrite(LCD_COMMAND, LCD_DDRAM + u8Row * LCD_LINE_OFFSET);
			for(u8Col = 0; u8Col < COL_NUMBER; u8Col++)
				LcdWrite(LCD_DATA, FilterSpecialChar(*pu8Text++));
		}
	}
}

// *****************************************************************************
/// @brief		Fun��o que centraliza um texto
/// @fn			void Centralize(u8 u8Line, u8 *pu8NumChar)
/// param[in]	u8Line		@brief Tamanho do fonte
/// param[in]	u8NumChar	@brief N�mero de caracteres
// *****************************************************************************
void Centralize(u8 u8Line, u8 *pu8NumChar)
{
	if (u8Line == LINE_2)
		u8Line = COL_NUMBER;
	if (*pu8NumChar > COL_NUMBER)
		*pu8NumChar = u8Line;
	else
		*pu8NumChar = (COL_NUMBER - *pu8NumChar) / 2 + u8Line;			// calcula a posi��o inicial para centralizar o texto
}

// *****************************************************************************
/// @brief		Fun��o que escreve um texto centralizado em uma linha do LCD
/// @fn			void LcdPrintf(u8 *pu8Dest, u8 u8Line, const char *fmt, ...)
/// @param[out]	pu8Dest		@brief Destino da imagem
/// @param[in]	u8Line		@brief Linha do LCD (1 ou 2)
// *****************************************************************************
void LcdPrintf(u8 *pu8Dest, u8 u8Line, const char *fmt, ...)
{
	va_list args;
	s8 as8LineBuffer[COL_NUMBER + 1];
	u8 u8Width;

	va_start(args, fmt);
	u8Width = vsnprintf((char*)as8LineBuffer, COL_NUMBER, fmt, args);
	va_end(args);
	Centralize(u8Line, &u8Width);
	strcpy((char*)&pu8Dest[u8Width], (char*)as8LineBuffer);
}

//**************** (C) COPYRIGHT 2015 Technomaster *******END OF FILE***********
