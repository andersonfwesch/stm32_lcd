//********************* (C) COPYRIGHT 2016 Technomaster ************************
/// @brief		Fun��es do sistema
/// @file 		system.c
/// @author		Paulo Steigleder
/// @version	V1.0.0
/// @date		08/03/2016
// *****************************************************************************

// Includes --------------------------------------------------------------------
//#include "stm32f10x.h"
#include "config.h"
#include "system.h"
#include "lcd.h"

// Private typedef -------------------------------------------------------------

// Private define --------------------------------------------------------------
#define UID_ADDRESS		0x1FFFF7E8
#define AD_V25			1.43f		// tens�o do sensor de temperatura a 25�C (V)
#define T_REF			25.0f		// temperatura de refer�ncia (�C)
#define T_SLOPE			4.30f		// sensibilidade do sensor de temperatura (mv/�C)
#define	VREF_AD			1.20f		// tens�o de refer�ncia (V)
#define AD_MAX			4095.0f		// valor m�ximo do conversor AD (12 bits)
#define VAD_FILTER_SIZE	100			// n�mero de termos dos filtros do conversor AD
#define VIN_R4			10.0f		// valor do resistor R4 (kOhm) do divisor de tens�o de alimenta��o
#define VIN_R3			1.0f		// valor do resistor R3 (kOhm) do divisor de tens�o de alimenta��o

// Private macro ---------------------------------------------------------------

// Private variables -----------------------------------------------------------
u8 bBlink;
u32 u32SystemTime;
float fVad[NUM_AD_CHANNELS];

// Private function prototypes -------------------------------------------------

// *****************************************************************************
/// @brief		Fun��o que retorna o per�odo de tempo entre uma vari�vel e o 
///           rel�gio do sistema
/// @fn			u32 TimeDiff(void)	
/// @retval		u32ElapsedTime @brief Per�odo de tempo decorrido (ms)	
// *****************************************************************************
u32 TimeDiff(u32 u32OldTime)
{
	u32 u32Now, u32ElapsedTime;

		u32Now = HAL_GetTick();
		if (u32Now >= u32OldTime)
			u32ElapsedTime = u32Now - u32OldTime;
		else
			u32ElapsedTime = 0xFFFFFFFF - u32OldTime + u32Now;
		return u32ElapsedTime;
}

// *****************************************************************************
/// @brief		Fun��o que gera um retardo em us
/// @fn			void Delay(u32 u32us)
/// @param[in]	u32us		@brief Retardo em us
// *****************************************************************************
void Delay_us(u32 u32us)
{
	u32 i;

	for (i = (SYSTEM_FREQUENCY / 24000000) * u32us; i; i--);
}

// *****************************************************************************
/// @brief		Fun��o que gera um tempo de espera
/// @fn			void Delay_ms(u32 u32Timeout)	
/// @param[in]	u32Timeout		@brief	tempo de espera (ms)
// *****************************************************************************
void Delay_ms(u32 u32Timeout)
{
	u32	u32Ref;

	u32Ref = HAL_GetTick();
	while (TimeDiff(u32Ref) < u32Timeout);
}

// *****************************************************************************
/// @brief		Fun��o que controla os piscas
/// @fn			void BlinkControl(void)	
// *****************************************************************************
void BlinkControl(void)
{
	static u32 u32TimerBlink = 0;

	if (TimeDiff(u32TimerBlink) >= 500)			// atualiza o pisca de 1 Hz
	{
		u32TimerBlink = HAL_GetTick();
		bBlink = (bool)!bBlink;
	}
}

// *****************************************************************************
/// @brief		Fun��o que retorna o status do pisca (1 Hz)
/// @fn			u8 IsBlink(void)	
// *****************************************************************************
u8 IsBlink(void)
{
	return bBlink;
}

// *****************************************************************************
/// @brief		Fun��o que l� o identificador �nico do processador
/// @fn			void GetUniqueID(u8 *pu8Pointer, u8 u8Size)
/// @param[in]	pu8Pointer		@brief Destino
/// @param[in]	u8Size			@brief N�mero de bytes a serem lidos
// *****************************************************************************
void GetUniqueID(u8 *pu8Pointer, u8 u8Size)
{
	u8 n;
	u8 *pu8SerialNumber;

	if (u8Size > UID_SIZE)
		u8Size = UID_SIZE;
	pu8SerialNumber = (u8*)(UID_ADDRESS + UID_SIZE - u8Size);
	for (n = 0; n < u8Size; n++)
		*pu8Pointer++ = *pu8SerialNumber++;
}

// *****************************************************************************
/// @brief		L� as entradas anal�gicas
/// @fn			void ReadAD(void)
// *****************************************************************************
//void ReadAD(void)
//{
//	static u32 u32VadAcum[NUM_AD_CHANNELS] = {0, 0, 0};
//	static u16 u16VadFilter = 0;
//
//	u8 n;
//
//	for (n = VIN; n < NUM_AD_CHANNELS; n++)
//		u32VadAcum[n] += ReadAnalogIn(n);
//	if (++u16VadFilter == VAD_FILTER_SIZE)
//	{
//		float fVref;
//
//		fVref = (float)u32VadAcum[VSYS] / VAD_FILTER_SIZE;
//		fVad[VIN] = VREF_AD * (VIN_R3 + VIN_R4) / VIN_R3 * (float)u32VadAcum[VIN] / VAD_FILTER_SIZE / fVref;
//		fVad[VSYS] = AD_MAX * VREF_AD / fVref;
//		fVad[TSYS] = (1000.0f * (AD_V25 - (float)u32VadAcum[TSYS] / VAD_FILTER_SIZE * VREF_AD / fVref)) / T_SLOPE + T_REF;
//		for (n = VIN; n < NUM_AD_CHANNELS; n++)
//			u32VadAcum[n] = 0;
//		u16VadFilter = 0;
//	}
//}

// *****************************************************************************
/// @brief		L� o canal anal�gico selecionado
/// @fn			float GetAD(u8 u8Channel)
/// param[in]	u8Channel	@brief	N�mero do canal anal�gico
/// @retval		@brief	Valor do canal anal�gico
// *****************************************************************************
float GetAD(u8 u8Channel)
{
	return fVad[u8Channel];
}

// *****************************************************************************
/// @brief		Controla a lumin�ncia do retro iluminador
/// @fn			void BacklightControl(u8 u8Value)
/// @param[in]	u8Value		@brief	Valor da lumin�ncia (0 a 9)
// *****************************************************************************
//void BacklightControl(u8 u8Value)
//{
//	if (u8Value < 100)
//		TIM_SetCompare1(TIM17, u8Value);
//	else
//		TIM_SetCompare1(TIM17, 100);
//}
//
//// *****************************************************************************
///// @brief		Controla o contraste do visor LCD
///// @fn			void ContrastControl(u8 u8Value)
///// @param[in]	u8Value		@brief	Valor do contraste (0 a 99)
//// *****************************************************************************
//void ContrastControl(u8 u8Value)
//{
//	if (u8Value < 100)
//		TIM_SetCompare1(TIM16, u8Value);
//	else
//		TIM_SetCompare1(TIM16, 100);
//}

// *****************************************************************************
/// @brief		Função que lê um porto de 16 bits
/// @fn			u16 HAL_GPIO_ReadPort(GPIO_TypeDef* GPIOx)
/// @param[in]	GPIOx		@brief Porto
/// @retval		Valor lido
// *****************************************************************************
u16 HAL_GPIO_ReadPort(GPIO_TypeDef* GPIOx)
{
	return GPIOx->IDR;
}

// *****************************************************************************
/// @brief		Função de escrita em um porto de 16 bits
/// @fn			u16 HAL_GPIO_WritePort(GPIO_TypeDef* GPIOx)
/// @param[in]	GPIOx		@brief Porto
/// @param[in]	PortVal: specifies the value to be written to the port output data register.
/// @retval		none
// *****************************************************************************
void HAL_GPIO_WritePort(GPIO_TypeDef* GPIOx, u16 PortVal)
{
	GPIOx->ODR = PortVal;
}

//**************** (C) COPYRIGHT 2016 Technomaster *******END OF FILE***********
